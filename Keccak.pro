#-------------------------------------------------
#
# Project created by QtCreator 2015-02-13T12:34:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Keccak
TEMPLATE = app


SOURCES += main.cpp\
        keccak.cpp \
         c/Source/Utils.c \
         c/Source/Generic.c \
         c/Source/Permutations.c \
         c/Source/Keccak_f.c \
         c/Source/Sponge.c \
         c/Source/MainKeccak.c \




HEADERS  += keccak.h \
         c/Headers/



INCLUDEPATH += /home/ceffa/Qt_projects/Keccak/c/Headers/


FORMS    += keccak.ui

DISTFILES +=

RESOURCES += \
    keccak.qrc
