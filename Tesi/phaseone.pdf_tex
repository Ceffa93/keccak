%% Creator: Inkscape inkscape 0.48.4, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'phaseone.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{1126.96777344bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,1.31687534)%
    \put(0,0){\includegraphics[width=\unitlength]{phaseone.pdf}}%
    \put(0.0007645,1.24290144){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{34}$
}}}%
    \put(0.14971607,1.24290144){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{31}$
}}}%
    \put(0.00193881,1.28990862){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{Phase 1}
}}}%
    \put(0.31485145,1.21032511){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Hash function}}}%
    \put(0.60625694,1.05186005){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{A[h/8]}}}%
    \put(0.47157076,1.09481124){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{4GB Array}}}%
    \put(0.81653324,0.77538677){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Hash table}}}%
    \put(0.34764438,0.69107195){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Table entry}}}%
    \put(0.3527483,0.6504715){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Key}}}%
    \put(0.35399611,0.60588104){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Data}}}%
    \put(0.3379999,1.15896568){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{512} \rightarrow 2^{35}$}}}%
    \put(0.79425916,0.84554124){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{If bit = 0}}}%
    \put(0.79333781,0.93809127){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Set bit = 1}}}%
    \put(0.56779373,0.63072176){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Add entry}}}%
    \put(0.39671964,0.96883188){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Get bit [h mod 8] }}}%
    \put(0.65288692,1.13060058){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$h$}}}%
    \put(0.00015721,1.21106796){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Inputs}}}%
    \put(0.14905788,1.21058546){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Outputs}}}%
    \put(0.57237361,0.71592467){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{If bit = 1}}}%
    \put(0.00725589,0.74858813){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Get $2^{34}$ bit index}}}%
    \put(0.0009217,0.4713204){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{34}$
}}}%
    \put(0.14987329,0.4713204){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{31}$
}}}%
    \put(0.00209602,0.52234322){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{Phase 2}
}}}%
    \put(0.54121127,0.36722009){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Hash table}}}%
    \put(0.72237204,0.32270659){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Table entry}}}%
    \put(0.72747596,0.28210611){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Key}}}%
    \put(0.72872378,0.23751561){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Data}}}%
    \put(0.29237841,0.26371518){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Search for key}}}%
    \put(0.00031442,0.43948692){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Inputs}}}%
    \put(0.1492151,0.43900442){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Outputs}}}%
    \put(0.0074131,0.07739782){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Get $2^{34}$ bit index}}}%
    \put(0.68767632,0.07609671){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{If not equals}}}%
    \put(0.6643787,0.01172951){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{Collision found}}}}%
  \end{picture}%
\endgroup%
