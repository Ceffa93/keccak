\clearpage
\section{This work: implementation, testing and optimization}\label{mywork}
\normalsize{

This chapter analyses our work on Keccak.

Section \ref{implementation} describes the implementation of Keccak-$f$ and the standard variants of Keccak.

Section \ref {mytrails} reports the algorithms to generate the differential trails for Keccak-$f$.

Section \ref{mysubset} first reports in details the attack implemented for the three-rounds version of Keccak-512, and its memory optimizations, then describes the attacks to two Keccak-160 variants, based on the original subset attack described in \cite{2}.\\*
\subsection{Keccak implementation}\label{implementation}
\normalsize{

Keccak was implemented entirely in the C language, on a notebook PC running Ubuntu 14.04.

The code for Keccak-$f$ and for the round constants generation with the LFSR for $\iota$ is contained in \texttt{Keccak\_f.c}, while the five rounds functions and the generation of shift and rotation values for $\pi$ and $\rho$ are implemented in  \texttt{Keccak\_f\_functions.c}.
The algorithm can be set up to behave like any of the Keccak-$f$[b] functions, with $b = 25 * 2^l$, $0\leq l\leq 7$, even though only Keccak-$f$[1600] is used by the standard sponge functions.

The implementation of the sponge construction is inside \texttt{Sponge.c}, and it can be set up with various parameters, like $r$, $c$, $b$, the padding or the hash output length.
The code is divided into two main functions, the absorbing phase and the squeezing phase.

Finally, inside \texttt{MainKeccak.c}, are implemented the algorithms for the four Keccak, the four SHA and the two Shake standard variants.
The only difference between this functions are the setup parameters they pass to the sponge construction, while Keccak-$f$ stays the same across all these versions.

A few parameters were also added to the final program, like the choice between ascii or binary input, or the length of the hash in the Shake variants.

We also made a GUI version of the program using Qt libraries, as shown in Figure \ref{fig:gui}
\begin{figure}[h!]
\centering
\includegraphics[width=0.9\textwidth ]{graphic/gui.png}
\caption{The GUI version of the Keccak implementation\\*\*\\*\label{fig:gui}}
\end{figure}
\*\\*

}
\subsection{Differential trails}\label{mytrails}
\normalsize{
In this work we implemented all the useful algorithms to generate three-rounds trails, as explained in section \ref{trails}.
The source code can be found in \texttt{Trails.c}.

The algorithm to generate trails with the method proposed by Duc et al. found 64 possible differences that lead to an optimal weight of 32.
The trails generation algorithm that uses vortices found 178 different trails with a weight of 36. It can be easily adapted to use 8-bits vortices, and a few collisions with the optimal weight of 48 were found. As already stated in Section \ref{trails}, no low-weight trails were found with 4-bits vortices.

The code also contains the implementation of a few useful functions to calculate the Hamming weight, propagation weight and reverse propagation weight of a state of Keccak.\\*
}
\subsection{Subset attack} \label{mysubset}
\normalsize{
In this section are described the practical attacks implemented in this work, based on the subset attack proposed in \cite{2} by Dinur, Dunkelman and Shamir.
First is described the naive version of the attack to Keccak-512, then are explained all the memory optimization techniques we used to make the attack practical on a desktop PC. These include a two-steps variant of the attack to cache possible collisions and an output reduction technique to further minimize the memory cost.
Finally are described the subset attacks adapted to two Keccak-160 variants.

In Appendix 1 is reported the source code of the practical three-rounds Keccak-512 attack described in this chapter.\\*
}
\subsubsection{\normalsize{Non-practical attack implementation}} 
\normalsize{
As described in Section \ref{subset_known}, the values chosen for the attack are $i = 4$ and a zero-characteristic.
The $2^{34}$ inputs are generated and Keccak-512 is run on each one of them.
The input-output pair is then stored in memory. If another identical output is already present, a collision is found.
We first tried to use red-black trees to handle memory, because the insert time has a best case scenario of $\Theta(1)$, compared to B-trees which always have an insert time of $\Theta(\log(n))$.
In the end we used an hash table instead, because it always grants an insertion and search time of $\Theta(1)$, requires less memory and has no rebalancing cost.
The time cost of hashing is irrelevant compared to its benefits.

The hash table used in the attack is called sparsehash, and the source code is in \texttt{libchash.c}.
As its name suggests, it is a sparse hash table, which grants a negligible memory overhead, a couple of bits for each entry on average.

Even with this memory optimization the attack is far from practical on a desktop PC.
The output of Keccak-512 is 512 bits long, while the input can be represented as a 34 bit index, instead of saving the whole matrix.
Therefore the entry size must be at least:
\begin{center}
$512/8 + 34/8 \approx 69$ B,
\end{center}
and the total RAM required amounts to about
\begin{center}
 $2^{34} * 69 / 2^{30} = 1104$ GB.
\end{center}
Potentially a collision could be found after only $2^{31}$ memory entries, but the required RAM would still be $\approx$ 136 GB.
On smaller variants of Keccak this attack could work without further optimizations, but for the three-rounds of Keccak-512 a two-steps attack was required to reduce memory usage.
This is described in the next section.\\*

\subsubsection{\normalsize{Practical attack implementation}}
\normalsize{ 
The number of hash table entries is the main reason that makes the attack heavily memory dependent.
So we reduced the number of entries saved in memory by dividing the attack in two phases:
\begin{description}
\item[$\cdot$]In the first phase possible collisions are detected and saved in memory.
\item[$\cdot$]In the second phase the actual collisions are found.
\end{description}
Detecting possible collisions during the first phase requires a cache array and a hash function with a fixed output size and a good diffusion.\\*
The hash function used in the attack, xxHash, can be found in \texttt{xxhash.c}.
It is very fast and provides a good diffusion, but any other hash function with these characteristics would work just as well.

At the beginning of the attack the array is preallocated, and its size is $2^n$ bits.
Then each output of Keccak-512 is calculated, but instead of being stored in memory, it is hashed with the hash function to produce a $n$ bits number.
This number is used as an index to access the cache array: $ n/8 $ is the position inside the bytes array, $n$ mod 8 is the bit offset inside the target byte.
The target bit is then set to 1, but if it already has a value of 1 the current input-output pair is stored in the sparse hash table described in the previous section, with the output as key.

Hashing the output caused an information loss, and multiple outputs could be mapped to the same hash, so the collision is just a possible collision.
It is important to note that there is no information left about the first of the two outputs that generated the same hash, so only the second input-output pair of each possible collision is saved to memory.
At the end of this first phase, the memory will contain all these input-output pairs, and the cache array will no longer be useful, so it can be freed.

The second phase requires to calculate again all the outputs of Keccak-512 for each input.
The outputs will be used to search the hash table.
When a match is found, the inputs must be compared: if they are different an actual collision is found, because two different inputs generated the same output.

With this system the memory usage is drastically reduced, and the only drawback is the doubling of the time of execution of the attack.
However, as described in the next section, the memory required will still be a little too much for an average desktop PC.
Reducing the number of inputs is an option, but it will also lower the chances of finding a collision.
The solution we adopted instead is to reduce the number of outputs.
The differences between the two approaches is that reducing the number of inputs will lead to an output subspace of the same size as before, but with less outputs stored in it. This reduces drastically the chances of finding a collision.
However if all the inputs are passed to Keccak-512, and then the outputs are reduced, the subspace will be smaller but with the same density as before.

An example to better understand this: if all the outputs with a bit 0 in the first position are discarded, the final subspace will be halved, because it won't contain outputs with a leading 1. Doing the same thing on the inputs will just half the memory entries, but their output space will be exactly the same as before, because it is impossible to know which outputs will be discarded. It is important to notice that with this method all the inputs must be processed, so the time of execution will be the same as the full attack.

In Figure \ref{fig:reducesize} are represented the two possible selection methods, and it is evident why selecting the outputs is better for a subset attack.\\*
\begin{figure}[h!]
\centering
\def\svgwidth{0.8\textwidth} 
\input{reducesize.pdf_tex}
\caption{The inputs and outputs reduction method. The latter reduces the output space.\\*\label{fig:reducesize}}
\end{figure}

\subsubsection{\normalsize{Memory optimization}}
\normalsize{ 
As described in the previous section, the practical attack requires two data structures: the preallocated cache array and the sparse hash table.
A small-size array generates too many false collisions, because many outputs will produce the same hash value.
On the other hand the memory cost of a big-size array is a problem by itself, and cannot justify the size reduction of the hash table.
To minimize the memory usage it is therefore necessary to balance these two data structures, but while the array size is known a priori, the hash table size must be estimated.
To do so, three steps are necessary:
\begin{center}
\begin{description}
\item[$\cdot$] 
Find the probability $P$ that an array's bit will be written more than one time,
\item[$\cdot$] 
Calculate the number of entries $n$ the hash table has, based on $p$,
\item[$\cdot$] 
Calculate the total size of the hash table based on $n$.
\end{description}
\end{center}
The first step requires the use of a Poisson distribution.
A value $x$ must be passed to the function, along with the average rate of success $r$, and the function will calculate the probability $p$ that a generic value $X$ is greater than or equal to $x$.
Referring to our problem, $p$ is the probability that a generic array cell $X$ will be written at least $x$ times.
With $x = 2$, $p$ is the probability that a generic array cell is written at least two times, thus adding a new entry to the hash table.

The average rate of success $r$ is simply the number of inputs divided by the size of the array (in bits):\\*
\begin{equation}\label{eq:poisson_rate}
r = \frac{n_i}{|\text{array}|}
\end{equation}

The second step requires to calculate the number of entries $n$ to the hash table. This is the probability $p$ multiplied by the number of array cells:
\begin{equation}\label{eq:poisson_n}
n = p \times |\text{array}|
\end{equation}
Actually $n$ is higher than that: Poisson calculate the probability that the number of outputs hashed to the same array cell is at least two, but if the number is, for example, three, two entries should be added instead of one.
Since this doesn't influence the result so much, and these estimates don't need to be incredibly precise, this will be ignored, but the final memory usage could be a little more than expected.
Finally, the total size of the hash table is $n$ multiplied by the size of a single hash table entry:
\begin{equation}\label{eq:hash_table_size}
|\text{hash table}|= n \times |\text{entry}|
\end{equation}
With these informations it is possible to calculate the total memory usage in bytes:
\normalsize{
\begin{equation}\label{eq:total_memory_usage}
|\text{array}| + P(r = \frac{n_i}{|\text{array}|}, x = 2, X \geq x) \times |\text{array}| \times |\text{entry}|
\end{equation}
}
\normalsize{

The only variable parameter in Formula \ref{eq:total_memory_usage} is the size of the array.
The other parameters are known, and for the attack to Keccak-512 previously described, the formula is:

\begin{equation}\label{eq:total_memory_usage_512}
|\text{array}| + P(r = \frac{2^{34}}{|\text{array}|}, x = 2, X \geq x) \times |\text{array}| \times 69
\end{equation}
The array size that minimizes this value is $2^{38}$, which occupies $2^{35}$ bytes, 32 GB. The total memory used amounts to 65 GB, which is a lot better than the 1104 GB required by the naive attack (which is actually a special case of the practical attack with $|\text{array}| = 2^0$ ).
Figure \ref{fig:poisson} represents the memory required by the attack by varying the array size.
\begin{figure}[h!]
\centering
\def\svgwidth{0.7\textwidth} 
\input{poisson.pdf_tex}
\caption{Memory usage of the attack with different array sizes.\\*\label{fig:poisson}}
\end{figure}
\*\\*
On an average desktop PC the attack is still not practical, so we were forced to use the output reduction method described in the previous section.
\begin{figure}[b!]
\centering
\def\svgwidth{\textwidth} 
\input{phaseone.pdf_tex}
\caption{Structure of the practical three-rounds Keccak-512 attack.\label{fig:phaseone}}
\end{figure}
The number of inputs $n_i$ in Formula \ref{eq:total_memory_usage} becomes a variable like the array size, and for each $n_i$ it is possible to calculate the lower memory usage by varying the array size.
The goal is to find the higher value of $n_i$ that can lead to a memory usage low enough to fit inside the RAM.
The PC used for the attack had 16 GB of RAM, so $n_i$ had to be reduced to one eight of the original value (still enough to obtain collisions, because with the outputs reduction method the density of the output subset is not reduced).

The total memory usage after the outputs reduction is:
\begin{equation}\label{eq:optimized_memory_512}
|\text{array}| + P(r = \frac{2^{34 - 3}}{|\text{array}|}, x = 2, X \geq x) \times |\text{array}| \times 69
\end{equation}
The value of $|\text{array}|$ that leads to the minimum memory usage is $2^{35}$, 4 GB,
with a practical total memory usage of 8.2 GB.

Figure \ref{fig:phaseone} represents the whole structure of the attack, divided in the two phases, and the values reported are relative to the version of the attack divided in eight parts.

The attack was implemented in \texttt{Attack.c}, and the most relevant code is reported in Appendix 1.\\*

\subsubsection{\normalsize{Time optimization}}
\normalsize{ 
The memory optimizations described in the previous section increase the time complexity of the algorithm:
\begin{description}
\item[$\cdot$] Two-steps attack: doubles the time
\item[$\cdot$] Output reductions in $n$ parts: multiplies the total time by $n$
\end{description}
To reduce the execution time of the attack as much as possible we used OpenMP directives to parallelize the code.
The main loop of both steps of the attack has been divided between multiple threads, effectively reducing the time by $k$ times, where $k$ is the machine-dependent number of parallel threads.
The use of the OpenMP directives is shown in the code excerpt of the attack in Appendix 1.\\*
}
\subsubsection{\normalsize{Results}}
With the optimizations described in the previous sections the attack was completed on a desktop PC in less than 8 hours, finding 15 collisions, the first one of them in less then 6 hours. Section \ref{attacksresults} reports an example of collision.
The attack was also run entirely on a remote server with 64 GB of RAM, reducing the outputs to one forth instead of one eight, thanks to the greater amount of RAM, and then running the four parts separately.
The whole attack found 158 collisions, and each part took less than 5 hours to complete.\\*
}

\subsubsection{\normalsize{Other attacks}}
\normalsize{ 
The authors of Keccak announced a "Crunchy Crypto contest" on the official Keccak website (\cite{11}).
It consists in a series of collision and preimage challenges on non-standard Keccak versions.
These variants are generally easier to attack than the ones submitted to the SHA-3 competition, and the hash length is just 160 bits for each one of them, while the bitrate is $b - 160$.
In this case $b$ is in $\{200, 400, 800, 1600\}$, while in the standard versions it is always 1600.
Also there is no padding in these variants.

We adapted the subset attack described in the previous section to two of these Keccak-160 variants, whose main difference is the Keccak-$f$[$b$] algorithm they use:
\begin{description}
\item[$\cdot$] The first one uses Keccak-$f$[400], $r = 240$, $c = 160$,
\item[$\cdot$] the second one Keccak-$f$[800], $r = 640$, $c = 160$.
\end{description}
From now on, to avoid confusion, I'll refer to these variants with the name of the Keccak-$f$ algorithm that characterizes them: Keccak-$f$[400] for the first one, Keccak-$f$[800] for the second one, however they are both Keccak-160.

The first thing to do is find the $i$ value that minimizes the number of inputs without reducing the probability of a collision.
To do so it is necessary to find how many output lanes are occupied by the hash:
\begin{description}
\item[$\cdot$] Keccak-$f$[400]: $\frac{160}{400/25} = 10$,
\item[$\cdot$] Keccak-$f$[800]: $\frac{160}{800/25} = 5$,
\end{description}
and how many lanes are occupied by the input:
\begin{description}
\item[$\cdot$] Keccak-$f$[400]: $\frac{240}{400/25} = 15$,
\item[$\cdot$] Keccak-$f$[800]: $\frac{640}{800/25} = 20$.
\end{description}
Then, similarly to Formula \ref{eq:choice_of_i}, it is possible to calculate the $i$ values:
\begin{description}
\item[$\cdot$] Keccak-$f$[400]: $2^{15*i} > \sqrt{2^{22 + (10 \times i)}}, $
\item[$\cdot$] Keccak-$f$[800]: $2^{20*i} > \sqrt{2^{22 + (5 \times i)}}. $
\end{description}
So the optimal $i$ values, the corresponding number of inputs $n_i$ and the subspace size are:
\begin{description}
\item[$\cdot$] Keccak-$f$[400]: $i=2$, with $n_i = 2^{30} > \sqrt{2^{42} = |\text{subset}|}$,
\item[$\cdot$] Keccak-$f$[800]: $i=1$, with $n_i = 2^{20} > \sqrt{2^{27} = |\text{subset}|}$.
\end{description}

It is interesting to notice that an high bitrate may lead to a worse security against this kind of attacks.
The second variant uses Keccak-$f$[800] as its core algorithm, which should be more secure than using Keccak-$f$[400].
However the bitrate $r$ is so high that with $i = 1$ there are already enough inputs to find a collision, and the output subspace is smaller too, because with an higher lane length $w$, the output hash is divided in bigger chunks and occupies less lanes.

The full attack to Keccak-$f$[400] required 30 minutes on a desktop PC and found more than two millions collisions, while the attack to Keccak-$f$[800] required less than one second, due to the very low number of inputs, and found $\sim 2.000$ collisions.
These high numbers of collisions were to be expected, because in both cases the number of inputs is a lot greater than the square root of the output subspace.

Both these attacks didn't use the output reduction method, because the memory usage was not as high as in the Keccak-512 attack.
Keccak-$f$[400] didn't even require the two-steps attack, because the number of inputs is so small, that every output can be saved directly to memory.

The code for both these attacks can be found in \texttt{Attack.c}. In Section \ref{attacksresults} will be presented an example of collision for both these attacks.\\*






}

