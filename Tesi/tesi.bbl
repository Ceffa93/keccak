% $ biblatex auxiliary file $
% $ biblatex version 2.3 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\entry{1}{misc}{}
  \name{author}{4}{}{%
    {{}%
     {Bertoni}{B.}%
     {G.}{G.}%
     {}{}%
     {}{}}%
    {{}%
     {Daemen}{D.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Peeters}{P.}%
     {M.}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Assche}{A.}%
     {G.~Van}{G.~V.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{BG+1}
  \strng{fullhash}{BGDJPMAGV1}
  \field{note}{\url{http://keccak.noekeon.org/Keccak-reference-3.0.pdf (2011)}}
  \field{title}{The {{\sc Keccak}} reference}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{2}{misc}{}
  \name{author}{2}{}{%
    {{}%
     {Dunkelman}{D.}%
     {Itai Dinur{,}~Orr}{I.~D.~O.}%
     {}{}%
     {}{}}%
    {{}%
     {Shamir}{S.}%
     {Adi}{A.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DIDOSA1}
  \strng{fullhash}{DIDOSA1}
  \field{howpublished}{Cryptology ePrint Archive, Report 2012/672}
  \field{note}{\url{http://eprint.iacr.org/2012/672.pdf (2012)}}
  \field{title}{Collision Attacks on Up to 5 Rounds of SHA-3 Using Generalized
  Internal Differentials}
\endentry

\entry{3}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Daemen}{D.}%
     {Joan}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Assche}{A.}%
     {Gilles~Van}{G.~V.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \strng{namehash}{DJAGV1}
  \strng{fullhash}{DJAGV1}
  \field{booktitle}{Procs of FSE 12 LNCS 7549}
  \verb{doi}
  \verb 10.1007/978-3-642-34047-5_24
  \endverb
  \field{pages}{422\bibrangedash 441}
  \field{title}{Differential Propagation Analysis of Keccak}
  \field{year}{2012}
\endentry

\entry{4}{misc}{}
  \name{author}{5}{}{%
    {{}%
     {Bertoni}{B.}%
     {G.}{G.}%
     {}{}%
     {}{}}%
    {{}%
     {Daemen}{D.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Peeters}{P.}%
     {M.}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Assche}{A.}%
     {G.~Van}{G.~V.}%
     {}{}%
     {}{}}%
    {{}%
     {Keer}{K.}%
     {R.~Van}{R.~V.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{BG+1}
  \strng{fullhash}{BGDJPMAGVKRV1}
  \field{note}{\url{http://keccak.noekeon.org/Keccak-implementation-3.2.pdf
  (2012)}}
  \field{title}{{{\sc Keccak}} implementation overview}
  \warn{\item Invalid format of field 'month'}
\endentry

\entry{5}{misc}{}
  \name{author}{4}{}{%
    {{}%
     {Morawiecki}{M.}%
     {Pawel}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Pieprzyk}{P.}%
     {Josef}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Srebrny}{S.}%
     {Marian}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Straus}{S.}%
     {Michal}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{MP+1}
  \strng{fullhash}{MPPJSMSM1}
  \field{howpublished}{Cryptology ePrint Archive, Report 2013/561}
  \field{note}{\url{https://eprint.iacr.org/2013/561.pdf (2013)}}
  \field{title}{Preimage attacks on the round-reduced Keccak with the aid of
  differential cryptanalysis}
\endentry

\entry{6}{misc}{}
  \name{author}{4}{}{%
    {{}%
     {Chang}{C.}%
     {Donghoon}{D.}%
     {}{}%
     {}{}}%
    {{}%
     {Kumar}{K.}%
     {Arnab}{A.}%
     {}{}%
     {}{}}%
    {{}%
     {Morawiecki}{M.}%
     {Pawell}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Sanadhya}{S.}%
     {Somitra~Kumar}{S.~K.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{CD+1}
  \strng{fullhash}{CDKAMPSSK1}
  \field{note}{\url{http://csrc.nist.gov/groups/ST/hash/sha-3/Aug2014/documents/chang_paper_sha3_2014_workshop.pdf
  (2014)}}
  \field{title}{1st and 2nd Preimage Attacks on 7, 8 and 9 Rounds of
  Keccak-224,256,384,512}
\endentry

\entry{7}{misc}{}
  \name{author}{2}{}{%
    {{}%
     {Dunkelman}{D.}%
     {Itai Dinur{,}~Orr}{I.~D.~O.}%
     {}{}%
     {}{}}%
    {{}%
     {Shamir}{S.}%
     {Adi}{A.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DIDOSA1}
  \strng{fullhash}{DIDOSA1}
  \field{howpublished}{Cryptology ePrint Archive, Report 2011/624}
  \field{note}{\url{https://eprint.iacr.org/2011/624.pdf (2011)}}
  \field{title}{New attacks on Keccak-224 and Keccak-256}
\endentry

\entry{8}{misc}{}
  \name{author}{5}{}{%
    {{}%
     {Dinur}{D.}%
     {Itai}{I.}%
     {}{}%
     {}{}}%
    {{}%
     {Morawiecki}{M.}%
     {Pawel}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Pieprzyk}{P.}%
     {Josef}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Srebrny}{S.}%
     {Marian}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Straus}{S.}%
     {Michal}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DI+1}
  \strng{fullhash}{DIMPPJSMSM1}
  \field{howpublished}{Cryptology ePrint Archive, Report 2014/259}
  \field{note}{\url{http://eprint.iacr.org/2014/259.pdf (2014)}}
  \field{title}{Practical Complexity Cube Attacks on Round-Reduced Keccak
  Sponge Function}
\endentry

\entry{9}{misc}{}
  \name{author}{2}{}{%
    {{}%
     {Morawiecki}{M.}%
     {Pawel}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Srebrny}{S.}%
     {Marian}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{MPSM1}
  \strng{fullhash}{MPSM1}
  \field{howpublished}{Cryptology ePrint Archive, Report 2010/285}
  \field{note}{\url{https://eprint.iacr.org/2010/285.pdf (2010)}}
  \field{title}{A SAT-based preimage analysis of reduced KECCAK hash functions}
\endentry

\entry{10}{misc}{}
  \field{note}{\url{http://keccak.noekeon.org/index.html} visited in october
  2015}
\endentry

\entry{11}{misc}{}
  \field{note}{\url{http://keccak.noekeon.org/crunchy_contest.html} visited in
  october 2015}
\endentry

\entry{12}{misc}{}
  \name{author}{5}{}{%
    {{}%
     {Dinur}{D.}%
     {Itai}{I.}%
     {}{}%
     {}{}}%
    {{}%
     {Morawiecki}{M.}%
     {Pawel}{P.}%
     {}{}%
     {}{}}%
    {{}%
     {Pieprzyk}{P.}%
     {Josef}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Srebrny}{S.}%
     {Marian}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Straus}{S.}%
     {Michal}{M.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{DI+1}
  \strng{fullhash}{DIMPPJSMSM1}
  \field{howpublished}{Cryptology ePrint Archive, Report 2014/736}
  \field{note}{\url{https://eprint.iacr.org/2014/736.pdf (2014)}}
  \field{title}{Cube Attacks and Cube-attack-like Cryptanalysis on the
  Round-reduced Keccak Sponge Function}
\endentry

\entry{13}{misc}{}
  \field{note}{\url{http://csrc.nist.gov/groups/ST/hash/sha-3/index.html}
  visited in october 2015}
\endentry

\entry{14}{misc}{}
  \field{note}{\url{http://www.club.di.unimi.it/people/ceffa.html} visited in
  october 2015}
\endentry

\lossort
\endlossort

\endinput
