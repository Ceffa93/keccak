#include <stdint.h>
struct Vertex{
	uint8_t x;
	uint8_t y;
};

struct Pi_square{
	struct Vertex a;
	struct Vertex b;
};

//Finds trails of weight 35 with the vortices method
void vortices_6(uint16_t w);

//Finds trails of weight 32 with the Duc method
void lowest_trail(uint16_t w);



uint64_t** custom_cube(char* s);
uint8_t check_cp_kernel(uint64_t** cube);
uint16_t pi_double_columns(struct Pi_square* v,uint8_t width);
uint16_t nr_active_rows(uint64_t** cube, uint8_t width);
uint16_t hamming_weight(uint64_t** cube, uint8_t width);
uint16_t propagation_weight(uint64_t** cube, uint8_t width);
uint16_t reverse_propagation_weight(uint64_t** cube, uint8_t width);


void insert_line(uint64_t** cube, uint8_t depth, uint8_t line, uint8_t number);




