#include <stdint.h>
#include <stdio.h>
#include "libchash.h"


//The main attack who finds collisions on 1/8 of subset
void keccak_512_three_rounds_attack(uint8_t fraction);

//Collisioni su keccak 800 con r = 640 
void keccak_challenge();

//Three round of keccak with only two cubes. In cube the result
void three_rounds(uint64_t** cube,uint64_t** cube2, int w);

//1.5 rounds, the result in cube2
void one_dot_five_rounds(uint64_t** cube,uint64_t** cube2, int l);


//Insert a cube from a seed
void insert_cube(uint64_t input_nr,uint64_t** cube);

//Funzione di inserimento per la challenge
void insert_cube_challenge_400(uint64_t input_nr,uint64_t** cube);
void insert_cube_challenge_400_ridotta(uint64_t input_nr,uint64_t** cube);

void insert_cube_challenge_800(uint64_t input_nr,uint64_t** cube);
void insert_cube_challenge_1440(uint64_t input_nr,uint64_t** cube);

void keccak_challenge_1440(uint8_t part);


//Fastest way to insert a lane
void set_lane(uint64_t n,uint64_t* lane);

//Copy th first cube lanes in the hash
void get_hash_from_cube(uint64_t *hash,uint64_t** cube);

//for the chalenge, to be refactored
void get_hash_from_1440_cube(uint64_t *hash,uint64_t** cube);
void get_400_hash_from_cube(uint64_t *hash,uint64_t** cube);
void get_800_hash_from_cube(uint64_t *hash,uint64_t** cube);



//Create copy of hash to be inserted in hash map
uint64_t* duplicate_hash(uint64_t* old_hash, int l);

//Badly written function to chek if two seeds have the same hash.
//Just to check if the collisions are good, no need for good code
void check_equals(uint64_t a, uint64_t b);
void check_equals_400(uint64_t a, uint64_t b);
void check_equals_800(uint64_t a, uint64_t b);

//Print only the first 576 bits of a cube (two lines)
void print_essential_cube(uint64_t** cube);

