#include <stdlib.h>
#include <stdint.h>
#include "Utils.h"
#include "Generic.h"
#include "keccak_f_functions.h"

uint64_t* ROUND_CONSTANTS;
uint8_t L;
uint8_t B;
uint8_t W;
uint8_t NR_ROUNDS;


void setup_keccak_f(uint8_t l);
void custom_setup_keccak_f(uint8_t l,uint8_t nr);
uint64_t ** keccak_f(uint64_t** cube);
uint64_t* round_constants();

