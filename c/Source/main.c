#include "MainKeccak.h"
#include "Trails.h"
#include "Attack.h"
#include <omp.h>


int main(int argc, char * argv[]){

//KECCAK VARIANTS (Pass a input txt file with: ./keccak < input.txt)
//Sha3 versions
// sha3_224();
// sha3_256();
// sha3_384();
// sha3_512();
//Keccak versions
// keccak_224();
// keccak_256();
// keccak_384();
// keccak_512();
//Shake versions
// shake_128(uint32_t shake_length);
// shake_256(uint32_t shake_length);

//TRAILS
//w = 32 trails by Duc et al.
//lowest_trail(6);
//w = 36 trials with vortices
//vortices_6(6);

//ATTACKS
//Keccak-512 attack (change argument to execute a different part of the attack)
//keccak_512_three_rounds_attack(0);
//Challenges attack on Keccak-160 
//keccak_challenge_400();
keccak_challenge_800();




return 0;
}


