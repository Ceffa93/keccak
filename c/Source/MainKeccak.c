#include "MainKeccak.h"



void setup(uint8_t l,uint16_t r,uint32_t hash_length,uint8_t sha_pad);

void setup(uint8_t l,uint16_t r,uint32_t hash_length,uint8_t sha_pad){
	
	setup_keccak_f(l);
	setup_sponge(25*(1<<l),r,hash_length,sha_pad);
}


void initialize(uint8_t type){
	initialize_sponge(type);
	
}
void print_all_hashes(uint32_t shake_length){
   
	
    print_hash(sha3_224());
    print_hash(sha3_256());
    print_hash(sha3_384());
    print_hash(sha3_512());
    print_hash(keccak_224());
    print_hash(keccak_256());
    print_hash(keccak_384());
    print_hash(keccak_512());
    print_hash(shake_128(shake_length));
    print_hash(shake_256(shake_length));

    

}


uint64_t* sha3_224(){
    printf("Sha3-224:\n\n");
	setup(6,1152,224,1);
	return sponge();
}

uint64_t*  sha3_256(){
    printf("\nSha3-256:\n\n");
	setup(6,1088,256,1);
	return sponge();
}

uint64_t*  sha3_384(){
    printf("\nSha3-384:\n\n");
	setup(6,832,384,1);
	return sponge();
}

uint64_t*  sha3_512(){
	
    printf("\nSha3-512:\n\n");
	
	setup(6,576,512,1);
	return sponge();
}

uint64_t*  keccak_224(){
	
    printf("\nKeccak-224:\n\n");
	
	setup(6,1152,224,0);
	return sponge();
}

uint64_t*  keccak_256(){
	
    printf("\nKeccak-256:\n\n");
	
	setup(6,1088,256,0);
	return sponge();
}

uint64_t*  keccak_384(){
	
	printf("\nKeccak-384:\n\n");
	
	setup(6,832,384,0);
	return sponge();
}

uint64_t*  keccak_512(){
	
	printf("\nKeccak-512:\n\n");
	
	setup(6,576,512,0);
	return sponge();
}

uint64_t* shake_128(uint32_t shake_length){
	
    printf("Shake-128:\n\n");
	
	setup(6,1344,shake_length,2);
	return sponge();
}

uint64_t* shake_256(uint32_t shake_length){
	
    printf("Shake-256:\n\n");
	
	setup(6,1088,shake_length,2);
	return sponge();
}





