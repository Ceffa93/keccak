#include "Keccak_f.h"


uint8_t change_state(uint64_t * state);

uint64_t ** keccak_f(uint64_t** cube){

		int i;

	for (i=0; i<NR_ROUNDS; i++){

		cube = theta(cube,W);

		cube = rho(cube,W);
		

		cube = pi(cube);
		

		cube = chi(cube);
		

		cube = iota(cube,ROUND_CONSTANTS,i);
		

	}
	return cube;

}

void setup_keccak_f(uint8_t l){

	L=l;
	W=1<<l;
	B=25*W;
	NR_ROUNDS = 12 + l * 2;
	ROUND_CONSTANTS = round_constants();

}

void custom_setup_keccak_f(uint8_t l,uint8_t nr){
	L=l;
	W=1<<l;
	B=25*W;
	NR_ROUNDS = nr;
	ROUND_CONSTANTS = round_constants();
}



uint64_t* round_constants(){
	uint64_t* rc = calloc(NR_ROUNDS,64);
	uint64_t state = 0x01;
	uint64_t bit_pos;
	uint8_t i,j;
	for (i=0; i<NR_ROUNDS;i++){
		rc[i]=0;
		for (j=0;j<7;j++){
			bit_pos = (1<<j) - 1;
			if (change_state(&state) && j<=L)
				rc[i]^=((uint64_t)1<<bit_pos);
		
		}


	}
	return rc;

}

uint8_t change_state(uint64_t * state){
	uint8_t result = ((*state) & 0x01)!=0;
	
	if (((*state) & 0x80) != 0)
		*state = ((*state) <<= 1) ^ 0x71;
	else
		(*state) <<= 1;
	return result;
}





