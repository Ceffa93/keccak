#include "Sponge.h"

uint8_t EXTRA_PADDING;
uint16_t R;
uint16_t C;
uint32_t NR_BITS;
uint8_t INPUT_TYPE;  //0 binary, 1 ascii, 2 hex



uint64_t** absorbing_phase();
uint64_t* squeezing_phase(uint64_t** cube);
uint64_t switch_to_lsb(uint64_t w);
void print_hash(uint64_t* hash);

void initialize_sponge(uint8_t type){
	INPUT_TYPE=type;
}

void setup_sponge(uint16_t b,uint16_t r,uint32_t hash_length,uint8_t sha_pad){

	R=r;
	C = b-R;
	NR_BITS = hash_length;
	EXTRA_PADDING = sha_pad;

}

uint64_t* sponge(){



	uint64_t** cube=absorbing_phase();
	uint64_t* hash;
	if (!cube){
		return NULL;
	}

	hash=squeezing_phase(cube);
	return hash;
	
	
	
	
}



uint64_t** absorbing_phase(){

	char ch;
	uint8_t x,y;
	uint16_t remaining_bits;
	uint8_t pad_index=0;
	int8_t n;

	uint8_t esc = 0;
	uint8_t padding = 0;
	
	uint8_t pad[3][5]={{1},{0,1,1},{1,1,1,1,1}};
	uint64_t** cube;
	uint64_t** old_cube = alloc_cube();

	while(!esc || !padding){

		remaining_bits = R;
		n=0;
		cube = alloc_cube();
		for(y=0;y<5;y++){
				for (x=0;x<5;x++){

					while(n<64){

						if (remaining_bits==0 || esc==1)
							break;			
						//file not read completely yet
						if (!padding){
							
							ch=getchar();
							if(ch==EOF){

								padding=1;
							
							}
							else if (INPUT_TYPE==1){
								if(ch=='1'){
									cube[x][y]^=(uint64_t)1<<n;
									n++;
									remaining_bits--;
                                 }
                                else if (ch=='0')
                                    n++;
                                else{
                                	free_cube(cube);
                                	free_cube(old_cube);

                                	return NULL;
                                }
								remaining_bits--;
							}
							else {
								                                	

									cube[x][y]^=(uint64_t)ch<<n;
									n+=8;
									remaining_bits-=8;
							}

						} else{

							//file finished, so it reads from the pad array
							if(pad[EXTRA_PADDING][pad_index]==1){
								cube[x][y]^=(uint64_t)1<<n;
							}
							n++;
							remaining_bits--;
							pad_index++;
							if (pad_index==(1+EXTRA_PADDING*2)){//it works because the 3 arrays are 1,3,5 long
								esc = 1;
								break;
							}
						}

					}
					
					n=0;														
					
				}
			}

			for(y=0;y<5;y++){
				for (x=0;x<5;x++){
					cube[x][y]^=old_cube[x][y];
				}
			}

			if(esc==1){
				cube[((R/64)+4)%5][R/(64*5)]^=0x8000000000000000;//precise offset of the last padding bit
			}



			old_cube=keccak_f(cube);

		}
		return old_cube;
}

uint64_t* squeezing_phase(uint64_t** cube){

	uint64_t* hash = calloc(NR_BITS/64, 64);
	uint32_t hash_index = 0;
	uint32_t r_index = 0;
	uint8_t x,y;
	while(1){

		for(y=0;y<5;y++){
			for (x=0;x<5;x++){
				if(hash_index<=NR_BITS/64 && r_index<=R){
					hash[hash_index]=cube[x][y];

					hash_index++;
					r_index+=64;
				}	
			}
		}
		if (hash_index<=NR_BITS/64){


			cube = keccak_f(cube);

			r_index=0;
		}else break;
	}

	print_hash(hash);
	return hash;
}

void print_hash(uint64_t* hash){
	if (!hash){
		printf("Invalid Input.\n\n");
		return;
	}

	uint16_t words_to_print = NR_BITS/64;
	uint8_t tail_to_print = NR_BITS%64;
	uint16_t counter = 0;
	while(counter<words_to_print){
		printf("%016lx\n",switch_to_lsb(hash[counter]) );
		counter++;
	}
	
	//with hash_length = 224 it must be printed half of the last word.
	//all the other standard length are divisible by 64
	if (tail_to_print!=0){
        printf("%x\n",(uint32_t)(switch_to_lsb(hash[counter])>>32) );
	}
	
	 printf("\n");
	 
	 }

uint64_t switch_to_lsb(uint64_t w) {
	uint64_t lsb;
	lsb= w>>56 | w<<56;
	lsb|= ( 0x00ff000000000000 & w<<40 )  | ( 0x000000000000ff00 & w>>40 );
	lsb|= ( 0x0000ff0000000000 & w<<24 )  | ( 0x0000000000ff0000 & w>>24 );
	lsb|= ( 0x000000ff00000000 & w<<8 )  | ( 0x00000000ff000000 & w>>8 );
    return lsb;
}
	

