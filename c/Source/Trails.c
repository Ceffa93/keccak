#include "Trails.h"
#include <Generic.h>
#include <keccak_f_functions.h>




uint8_t check_correct_columns(uint64_t** cube, uint8_t width);




//easily create a keccak 25 cube for testing purposes
uint64_t** custom_cube(char* s){
	uint64_t** cube = alloc_cube();
	uint8_t y,x;
	uint8_t c=0;
	
	for(y=0;y<5;y++){
		for(x=0;x<5;x++){
			if(s[c]=='1') 
				cube[x][y]=1;
			c++;
			
		}
		c++;
	}
	return cube;
}

//this function check if the cube is in the teta cp kernel
uint8_t check_cp_kernel(uint64_t** cube){
	uint8_t x,y;
	uint8_t count=0;
	
	for(x=0;x<5;x++){
		for(y=0;y<5;y++){
			count+=cube[x][y];
		}
		if (count%2!=0) return 0;
	}
	return 1;

}

//check the 4 bit input configurations (belonging to cp kernel) that after pi are input for X-zero trails
//I build all the cubes with bits displayed as squares or rectangles.
//Then pi is applied.
//Then the 4 remaining bits must be in two columns and on 4 different rows. If not, the configuration is discarded
uint16_t pi_double_columns(struct Pi_square* v,uint8_t width){

	uint8_t a_x,a_y,b_x,b_y;
	uint32_t totale = 0;
	uint64_t** new_cube;
	uint64_t** cube;
	cube = alloc_cube();
	//for each position of the first vertex
	for(a_x=0;a_x<5;a_x++){
			for(a_y=0;a_y<5;a_y++){
			//and for each position of the second vertex
			for(b_x=a_x+1;b_x<5;b_x++){
				for(b_y=a_y+1;b_y<5;b_y++){
				 cube = wipe_cube(cube);
				cube[a_x][a_y]=1;
				cube[b_x][a_y]=1;
				cube[a_x][b_y]=1;
				cube[b_x][b_y]=1;
				new_cube = pi(cube);

				if(check_correct_columns(new_cube,width)){
					totale++;
					v = realloc(v,totale * sizeof(struct Pi_square));
					print_binary_cube(new_cube,3);
					v[totale-1].a.x=a_x;
					v[totale-1].a.y=a_y;
					v[totale-1].b.x=b_x;
					v[totale-1].b.y=b_y;
					}
					free_cube(new_cube);
				}
			}
		}
	}

	free_cube(cube);
	return totale;
}

//it verify that the "after pi" configuration lead to x-zero trail

uint8_t check_correct_columns(uint64_t** cube, uint8_t width){
	uint16_t x,y,z,y_ptr;
	uint16_t current = 99;

	
		for(y=0;y<5;y++){
			for(x=0;x<5;x++){
				for(z=0;z<width;z++)

				if((cube[x][y]&int_pow(2,z))==1){
					if(y==current) return 0;
					current = y;
				}
			}
		}
		return 1;
}



// ||a||row
uint16_t nr_active_rows(uint64_t** cube, uint8_t width){
	uint16_t x,y,z,total=0;
for (z=0;z<width; z++)
		for(y=0;y<5;y++){
			for(x=0;x<5;x++){
				if((cube[x][y]&int_pow(2,z))!=0){
					total++;
					break;
				}
			}
		}
		return total;
}
// ||a||  number of active bits
uint16_t hamming_weight(uint64_t** cube, uint8_t width){
	uint16_t x,y,z,total=0;

		for(y=0;y<5;y++){
			for(x=0;x<5;x++){
				for (z=0;z<width; z++){



				if((cube[x][y]&int_pow(2,z))!=0)
					total++;
			}
		}
		}
		return total;
}


// w(a)  number of active bits
uint16_t propagation_weight(uint64_t** cube, uint8_t width){
	uint16_t x,y,z,active_cnt=0,total=0;
	uint16_t zero_1,zero_2;
	for(z=0;z<width;z++){
		for(y=0;y<5;y++){
			for(x=0;x<5;x++){

				if((cube[x][y]&int_pow(2,z))!=0)
					active_cnt++;
				else{
					zero_2 = zero_1;
					zero_1 = x;
				}
					
				
				
			}
			switch (active_cnt){
				case 0: break;
				case 1: total+=2;break;
				case 2: total+=3;break;
				case 3:
					if (zero_1-zero_2==1 || zero_1-zero_2==4)
						total+=4;
					else total+=3;
					break;
				case 4: total+=4; break;
				case 5: total+=4; break;
			}
			active_cnt=0;

		}
	}
		return total;
	
}

// w(a)  number of active bits
uint16_t reverse_propagation_weight(uint64_t** cube, uint8_t width){
	uint8_t x,y,z,active_cnt=0,total=0;
	uint8_t zero_1,zero_2;
	for(z=0;z<width;z++){
		for(y=0;y<5;y++){
			for(x=0;x<5;x++){

			if((cube[x][y]&int_pow(2,z))!=0)
					active_cnt++;
				else{
					zero_2 = zero_1;
					zero_1 = x;
				}
					
				
				
			}
			switch (active_cnt){
				case 0: break;
				case 1: total+=2;break;
				case 2: total+=2;break;
				case 3:
					if (zero_1-zero_2==1 || zero_1-zero_2==4)
						total+=2;
					else total+=3;
					break;
				case 4: total+=3; break;
				case 5: total+=3; break;
			}
			active_cnt=0;
		}
		}
		return total;
	
}

void vortices_6(uint16_t w){
	printf("**6-Vortices Trails (Weight = 36)**\n\n\n");
	uint8_t a_x,a_y,b_x,b_y,c_x, c_y, d_x, d_y;
	uint32_t tot=0;

	int start = 0;
	
	uint64_t** cube = alloc_cube();

	for(a_x=0;a_x<5;a_x++){
	for(a_y=0;a_y<5;a_y++){
		for(b_x=a_x+1;b_x<5;b_x++){
		//for(b_x=0;b_x<5;b_x++){
		for(b_y=0;b_y<5;b_y++){
		if(a_y!=b_y){
		//if(a_x!= b_x && a_y!=b_y){
			for(c_x = b_x+1;c_x<5;c_x++){
			//for(c_x=0;c_x<5;c_x++){
			for(c_y=0;c_y<5;c_y++){
			if(c_y!=b_y && c_y!=a_y){

			// for(d_x = c_x+1;d_x<5;d_x++){
			// //for(c_x=0;c_x<5;c_x++){
			// for(d_y=0;d_y<5;d_y++){
			// if(d_y != a_y && d_y!=c_y){
		
				int32_t result = rho_rotations[a_x][a_y] - rho_rotations[a_x][b_y] + rho_rotations[b_x][b_y]- rho_rotations[b_x][c_y] + rho_rotations[c_x][c_y] - rho_rotations[c_x][a_y];
				//int32_t result = rho_rotations[a_x][a_y] - rho_rotations[a_x][b_y] + rho_rotations[b_x][b_y]- rho_rotations[b_x][c_y] + rho_rotations[c_x][c_y] - rho_rotations[c_x][d_y] + rho_rotations[d_x][d_y] - rho_rotations[d_x][a_y];
  				result = result>0? result : -result;

  				if((result%64)==0){
  					
  					
  					//printf("%d:%d - %d:%d - %d:%d\n",a_x,a_y,b_x,b_y,c_x, c_y);
  					int d1,d2,d3;
  					for (d1 = 0; d1<64; d1++){
  					for (d2 = d1+1; d2<64; d2++){
  					for (d3 = d2+1; d3<64; d3++){
						cube[a_x][a_y]=(uint64_t)1<<d1;
						cube[a_x][b_y] = (uint64_t)1<<d1;
						cube[b_x][b_y]=(uint64_t)1<<d2;
						cube[b_x][c_y] = (uint64_t)1<<d2;
						cube[c_x][c_y]=(uint64_t)1<<d3;
						cube[c_x][a_y] = (uint64_t)1<<d3;
						// /print_hex_cube(cube);
						int w0 = propagation_weight(cube,64);
						if(w0<=12){
							
								
							cube=  theta(cube, 64);
							cube =  rho(cube, 64);
							cube =  pi(cube);
							int w1 = propagation_weight(cube,64);
							if(w1<=12){
								

								cube=  theta(cube, 64);
								cube =  rho(cube, 64);
								cube =  pi(cube);
									
							

								//cube =  chi(cube);
								
								int w2 = propagation_weight(cube,64);
								if(w2<=12){
									tot++;
									
									printf("[Trail %d]\n",tot);
									printf("\tPeso: %d\n",w0+w1+w2);
									printf("\tPesi parziali: %d,%d,%d\n",w0,w1,w2);
									printf("\tVertices coordinates (x,y,z):\n");
									printf("\t(%d,%d,%d)\n",a_x,a_y,d1);
									printf("\t(%d,%d,%d)\n",a_x,b_y,d1);
									printf("\t(%d,%d,%d)\n",b_x,b_y,d2);
									printf("\t(%d,%d,%d)\n",b_x,c_y,d2);
									printf("\t(%d,%d,%d)\n",c_x,c_y,d3);
									printf("\t(%d,%d,%d)\n\n",c_x,a_y,d3);

									
									
								}
							}
						}
						wipe_cube(cube);
					}
					}
					}
					
				}
			}
			}	
			}//}}}
		}
		}
		}
	}
	}
	
}


void lowest_trail(uint16_t w){
	printf("**Lowest-weight trails (Weight = 32)**\n\n\n");

	uint8_t x,y1,y2;
	uint32_t tot = 0;
	uint64_t** cube = alloc_cube();

	for(x=0;x<5;x++){
	for(y1=0;y1<5;y1++){
	for(y2=y1+1;y2<5;y2++){
		//	printf("[%d:%d] - [%d:%d]\n",x,y1,x,y2);
			int d;
			for (d = 0; d<64; d++){
			
			cube[x][y1]=(uint64_t)1<<d;
			cube[x][y2]=(uint64_t)1<<d;
			
			// /print_hex_cube(cube);
			int w0 = propagation_weight(cube,64);
			if(w0<=4){
			
				cube=  theta(cube, 64);
				cube =  rho(cube, 64);
				cube =  pi(cube);
				int w1 = propagation_weight(cube,64);
				if(w1<=4){
				
					
					cube=  theta(cube, 64);
					cube =  rho(cube, 64);
					cube =  pi(cube);
				
					
					int w2 = propagation_weight(cube,64);
					if(w2<=30){

						tot++;
						printf("[Trail %d]\n",tot);
						printf("\tPeso: %d\n",w0+w1+w2);
						printf("\tPesi parziali: %d,%d,%d\n",w0,w1,w2);
						printf("\tVertices coordinates (x,y,z):\n");
						printf("\t(%d,%d,%d)\n",x,y1,d);
						printf("\t(%d,%d,%d)\n\n",x,y1,d);
						

						
					}
				}
			}
			wipe_cube(cube);
		}
	}	
	}
	}
}


	
