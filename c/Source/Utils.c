#include "Utils.h"

uint64_t int_pow (uint64_t b, uint64_t e){
	if (e==0) 
		return 1;
	uint8_t i;
	uint64_t s = b;
	for(i=1;i<e;i++){
		s*=b;
	}
	return s;
}

void print_binary(uint64_t n,uint8_t l){
	uint64_t bit = int_pow(2,(int_pow(2,l))-1);
	while (bit!=0){
		printf("%d", n & bit ? 1 : 0);
		bit/=2;
	}
}