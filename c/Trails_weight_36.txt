**6-Vortices Trails (Weight = 36)**


[Trail 1]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,0)
	(0,1,0)
	(2,1,30)
	(2,3,30)
	(4,3,37)
	(4,0,37)

[Trail 2]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,1)
	(0,1,1)
	(2,1,31)
	(2,3,31)
	(4,3,38)
	(4,0,38)

[Trail 3]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,2)
	(0,1,2)
	(2,1,32)
	(2,3,32)
	(4,3,39)
	(4,0,39)

[Trail 4]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,3)
	(0,1,3)
	(2,1,33)
	(2,3,33)
	(4,3,40)
	(4,0,40)

[Trail 5]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,4)
	(0,1,4)
	(2,1,34)
	(2,3,34)
	(4,3,41)
	(4,0,41)

[Trail 6]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,5)
	(0,1,5)
	(2,1,35)
	(2,3,35)
	(4,3,42)
	(4,0,42)

[Trail 7]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,6)
	(0,1,6)
	(2,1,36)
	(2,3,36)
	(4,3,43)
	(4,0,43)

[Trail 8]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,7)
	(0,1,7)
	(2,1,37)
	(2,3,37)
	(4,3,44)
	(4,0,44)

[Trail 9]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,8)
	(0,1,8)
	(2,1,38)
	(2,3,38)
	(4,3,45)
	(4,0,45)

[Trail 10]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,9)
	(0,1,9)
	(2,1,39)
	(2,3,39)
	(4,3,46)
	(4,0,46)

[Trail 11]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,10)
	(0,1,10)
	(2,1,40)
	(2,3,40)
	(4,3,47)
	(4,0,47)

[Trail 12]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,11)
	(0,1,11)
	(2,1,41)
	(2,3,41)
	(4,3,48)
	(4,0,48)

[Trail 13]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,12)
	(0,1,12)
	(2,1,42)
	(2,3,42)
	(4,3,49)
	(4,0,49)

[Trail 14]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,13)
	(0,1,13)
	(2,1,43)
	(2,3,43)
	(4,3,50)
	(4,0,50)

[Trail 15]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,14)
	(0,1,14)
	(2,1,44)
	(2,3,44)
	(4,3,51)
	(4,0,51)

[Trail 16]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,15)
	(0,1,15)
	(2,1,45)
	(2,3,45)
	(4,3,52)
	(4,0,52)

[Trail 17]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,16)
	(0,1,16)
	(2,1,46)
	(2,3,46)
	(4,3,53)
	(4,0,53)

[Trail 18]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,17)
	(0,1,17)
	(2,1,47)
	(2,3,47)
	(4,3,54)
	(4,0,54)

[Trail 19]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,18)
	(0,1,18)
	(2,1,48)
	(2,3,48)
	(4,3,55)
	(4,0,55)

[Trail 20]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,19)
	(0,1,19)
	(2,1,49)
	(2,3,49)
	(4,3,56)
	(4,0,56)

[Trail 21]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,20)
	(0,1,20)
	(2,1,50)
	(2,3,50)
	(4,3,57)
	(4,0,57)

[Trail 22]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,21)
	(0,1,21)
	(2,1,51)
	(2,3,51)
	(4,3,58)
	(4,0,58)

[Trail 23]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,22)
	(0,1,22)
	(2,1,52)
	(2,3,52)
	(4,3,59)
	(4,0,59)

[Trail 24]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,23)
	(0,1,23)
	(2,1,53)
	(2,3,53)
	(4,3,60)
	(4,0,60)

[Trail 25]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,24)
	(0,1,24)
	(2,1,54)
	(2,3,54)
	(4,3,61)
	(4,0,61)

[Trail 26]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,25)
	(0,1,25)
	(2,1,55)
	(2,3,55)
	(4,3,62)
	(4,0,62)

[Trail 27]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,26)
	(0,1,26)
	(2,1,56)
	(2,3,56)
	(4,3,63)
	(4,0,63)

[Trail 28]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,0)
	(0,4,0)
	(2,4,21)
	(2,1,21)
	(3,1,36)
	(3,0,36)

[Trail 29]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,1)
	(0,4,1)
	(2,4,22)
	(2,1,22)
	(3,1,37)
	(3,0,37)

[Trail 30]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,2)
	(0,4,2)
	(2,4,23)
	(2,1,23)
	(3,1,38)
	(3,0,38)

[Trail 31]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,3)
	(0,4,3)
	(2,4,24)
	(2,1,24)
	(3,1,39)
	(3,0,39)

[Trail 32]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,4)
	(0,4,4)
	(2,4,25)
	(2,1,25)
	(3,1,40)
	(3,0,40)

[Trail 33]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,5)
	(0,4,5)
	(2,4,26)
	(2,1,26)
	(3,1,41)
	(3,0,41)

[Trail 34]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,6)
	(0,4,6)
	(2,4,27)
	(2,1,27)
	(3,1,42)
	(3,0,42)

[Trail 35]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,7)
	(0,4,7)
	(2,4,28)
	(2,1,28)
	(3,1,43)
	(3,0,43)

[Trail 36]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,8)
	(0,4,8)
	(2,4,29)
	(2,1,29)
	(3,1,44)
	(3,0,44)

[Trail 37]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,9)
	(0,4,9)
	(2,4,30)
	(2,1,30)
	(3,1,45)
	(3,0,45)

[Trail 38]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,10)
	(0,4,10)
	(2,4,31)
	(2,1,31)
	(3,1,46)
	(3,0,46)

[Trail 39]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,11)
	(0,4,11)
	(2,4,32)
	(2,1,32)
	(3,1,47)
	(3,0,47)

[Trail 40]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,12)
	(0,4,12)
	(2,4,33)
	(2,1,33)
	(3,1,48)
	(3,0,48)

[Trail 41]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,13)
	(0,4,13)
	(2,4,34)
	(2,1,34)
	(3,1,49)
	(3,0,49)

[Trail 42]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,14)
	(0,4,14)
	(2,4,35)
	(2,1,35)
	(3,1,50)
	(3,0,50)

[Trail 43]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,15)
	(0,4,15)
	(2,4,36)
	(2,1,36)
	(3,1,51)
	(3,0,51)

[Trail 44]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,16)
	(0,4,16)
	(2,4,37)
	(2,1,37)
	(3,1,52)
	(3,0,52)

[Trail 45]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,17)
	(0,4,17)
	(2,4,38)
	(2,1,38)
	(3,1,53)
	(3,0,53)

[Trail 46]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,18)
	(0,4,18)
	(2,4,39)
	(2,1,39)
	(3,1,54)
	(3,0,54)

[Trail 47]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,19)
	(0,4,19)
	(2,4,40)
	(2,1,40)
	(3,1,55)
	(3,0,55)

[Trail 48]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,20)
	(0,4,20)
	(2,4,41)
	(2,1,41)
	(3,1,56)
	(3,0,56)

[Trail 49]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,21)
	(0,4,21)
	(2,4,42)
	(2,1,42)
	(3,1,57)
	(3,0,57)

[Trail 50]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,22)
	(0,4,22)
	(2,4,43)
	(2,1,43)
	(3,1,58)
	(3,0,58)

[Trail 51]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,23)
	(0,4,23)
	(2,4,44)
	(2,1,44)
	(3,1,59)
	(3,0,59)

[Trail 52]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,24)
	(0,4,24)
	(2,4,45)
	(2,1,45)
	(3,1,60)
	(3,0,60)

[Trail 53]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,25)
	(0,4,25)
	(2,4,46)
	(2,1,46)
	(3,1,61)
	(3,0,61)

[Trail 54]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,26)
	(0,4,26)
	(2,4,47)
	(2,1,47)
	(3,1,62)
	(3,0,62)

[Trail 55]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,0,27)
	(0,4,27)
	(2,4,48)
	(2,1,48)
	(3,1,63)
	(3,0,63)

[Trail 56]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,0)
	(0,4,0)
	(2,4,21)
	(2,3,21)
	(4,3,28)
	(4,2,28)

[Trail 57]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,1)
	(0,4,1)
	(2,4,22)
	(2,3,22)
	(4,3,29)
	(4,2,29)

[Trail 58]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,2)
	(0,4,2)
	(2,4,23)
	(2,3,23)
	(4,3,30)
	(4,2,30)

[Trail 59]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,3)
	(0,4,3)
	(2,4,24)
	(2,3,24)
	(4,3,31)
	(4,2,31)

[Trail 60]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,4)
	(0,4,4)
	(2,4,25)
	(2,3,25)
	(4,3,32)
	(4,2,32)

[Trail 61]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,5)
	(0,4,5)
	(2,4,26)
	(2,3,26)
	(4,3,33)
	(4,2,33)

[Trail 62]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,6)
	(0,4,6)
	(2,4,27)
	(2,3,27)
	(4,3,34)
	(4,2,34)

[Trail 63]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,7)
	(0,4,7)
	(2,4,28)
	(2,3,28)
	(4,3,35)
	(4,2,35)

[Trail 64]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,8)
	(0,4,8)
	(2,4,29)
	(2,3,29)
	(4,3,36)
	(4,2,36)

[Trail 65]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,9)
	(0,4,9)
	(2,4,30)
	(2,3,30)
	(4,3,37)
	(4,2,37)

[Trail 66]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,10)
	(0,4,10)
	(2,4,31)
	(2,3,31)
	(4,3,38)
	(4,2,38)

[Trail 67]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,11)
	(0,4,11)
	(2,4,32)
	(2,3,32)
	(4,3,39)
	(4,2,39)

[Trail 68]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,12)
	(0,4,12)
	(2,4,33)
	(2,3,33)
	(4,3,40)
	(4,2,40)

[Trail 69]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,13)
	(0,4,13)
	(2,4,34)
	(2,3,34)
	(4,3,41)
	(4,2,41)

[Trail 70]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,14)
	(0,4,14)
	(2,4,35)
	(2,3,35)
	(4,3,42)
	(4,2,42)

[Trail 71]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,15)
	(0,4,15)
	(2,4,36)
	(2,3,36)
	(4,3,43)
	(4,2,43)

[Trail 72]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,16)
	(0,4,16)
	(2,4,37)
	(2,3,37)
	(4,3,44)
	(4,2,44)

[Trail 73]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,17)
	(0,4,17)
	(2,4,38)
	(2,3,38)
	(4,3,45)
	(4,2,45)

[Trail 74]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,18)
	(0,4,18)
	(2,4,39)
	(2,3,39)
	(4,3,46)
	(4,2,46)

[Trail 75]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,19)
	(0,4,19)
	(2,4,40)
	(2,3,40)
	(4,3,47)
	(4,2,47)

[Trail 76]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,20)
	(0,4,20)
	(2,4,41)
	(2,3,41)
	(4,3,48)
	(4,2,48)

[Trail 77]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,21)
	(0,4,21)
	(2,4,42)
	(2,3,42)
	(4,3,49)
	(4,2,49)

[Trail 78]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,22)
	(0,4,22)
	(2,4,43)
	(2,3,43)
	(4,3,50)
	(4,2,50)

[Trail 79]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,23)
	(0,4,23)
	(2,4,44)
	(2,3,44)
	(4,3,51)
	(4,2,51)

[Trail 80]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,24)
	(0,4,24)
	(2,4,45)
	(2,3,45)
	(4,3,52)
	(4,2,52)

[Trail 81]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,25)
	(0,4,25)
	(2,4,46)
	(2,3,46)
	(4,3,53)
	(4,2,53)

[Trail 82]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,26)
	(0,4,26)
	(2,4,47)
	(2,3,47)
	(4,3,54)
	(4,2,54)

[Trail 83]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,27)
	(0,4,27)
	(2,4,48)
	(2,3,48)
	(4,3,55)
	(4,2,55)

[Trail 84]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,28)
	(0,4,28)
	(2,4,49)
	(2,3,49)
	(4,3,56)
	(4,2,56)

[Trail 85]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,29)
	(0,4,29)
	(2,4,50)
	(2,3,50)
	(4,3,57)
	(4,2,57)

[Trail 86]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,30)
	(0,4,30)
	(2,4,51)
	(2,3,51)
	(4,3,58)
	(4,2,58)

[Trail 87]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,31)
	(0,4,31)
	(2,4,52)
	(2,3,52)
	(4,3,59)
	(4,2,59)

[Trail 88]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,32)
	(0,4,32)
	(2,4,53)
	(2,3,53)
	(4,3,60)
	(4,2,60)

[Trail 89]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,33)
	(0,4,33)
	(2,4,54)
	(2,3,54)
	(4,3,61)
	(4,2,61)

[Trail 90]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,34)
	(0,4,34)
	(2,4,55)
	(2,3,55)
	(4,3,62)
	(4,2,62)

[Trail 91]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,2,35)
	(0,4,35)
	(2,4,56)
	(2,3,56)
	(4,3,63)
	(4,2,63)

[Trail 92]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,0)
	(0,0,0)
	(2,0,2)
	(2,2,2)
	(3,2,20)
	(3,3,20)

[Trail 93]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,1)
	(0,0,1)
	(2,0,3)
	(2,2,3)
	(3,2,21)
	(3,3,21)

[Trail 94]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,2)
	(0,0,2)
	(2,0,4)
	(2,2,4)
	(3,2,22)
	(3,3,22)

[Trail 95]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,3)
	(0,0,3)
	(2,0,5)
	(2,2,5)
	(3,2,23)
	(3,3,23)

[Trail 96]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,4)
	(0,0,4)
	(2,0,6)
	(2,2,6)
	(3,2,24)
	(3,3,24)

[Trail 97]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,5)
	(0,0,5)
	(2,0,7)
	(2,2,7)
	(3,2,25)
	(3,3,25)

[Trail 98]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,6)
	(0,0,6)
	(2,0,8)
	(2,2,8)
	(3,2,26)
	(3,3,26)

[Trail 99]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,7)
	(0,0,7)
	(2,0,9)
	(2,2,9)
	(3,2,27)
	(3,3,27)

[Trail 100]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,8)
	(0,0,8)
	(2,0,10)
	(2,2,10)
	(3,2,28)
	(3,3,28)

[Trail 101]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,9)
	(0,0,9)
	(2,0,11)
	(2,2,11)
	(3,2,29)
	(3,3,29)

[Trail 102]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,10)
	(0,0,10)
	(2,0,12)
	(2,2,12)
	(3,2,30)
	(3,3,30)

[Trail 103]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,11)
	(0,0,11)
	(2,0,13)
	(2,2,13)
	(3,2,31)
	(3,3,31)

[Trail 104]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,12)
	(0,0,12)
	(2,0,14)
	(2,2,14)
	(3,2,32)
	(3,3,32)

[Trail 105]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,13)
	(0,0,13)
	(2,0,15)
	(2,2,15)
	(3,2,33)
	(3,3,33)

[Trail 106]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,14)
	(0,0,14)
	(2,0,16)
	(2,2,16)
	(3,2,34)
	(3,3,34)

[Trail 107]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,15)
	(0,0,15)
	(2,0,17)
	(2,2,17)
	(3,2,35)
	(3,3,35)

[Trail 108]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,16)
	(0,0,16)
	(2,0,18)
	(2,2,18)
	(3,2,36)
	(3,3,36)

[Trail 109]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,17)
	(0,0,17)
	(2,0,19)
	(2,2,19)
	(3,2,37)
	(3,3,37)

[Trail 110]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,18)
	(0,0,18)
	(2,0,20)
	(2,2,20)
	(3,2,38)
	(3,3,38)

[Trail 111]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,19)
	(0,0,19)
	(2,0,21)
	(2,2,21)
	(3,2,39)
	(3,3,39)

[Trail 112]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,20)
	(0,0,20)
	(2,0,22)
	(2,2,22)
	(3,2,40)
	(3,3,40)

[Trail 113]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,21)
	(0,0,21)
	(2,0,23)
	(2,2,23)
	(3,2,41)
	(3,3,41)

[Trail 114]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,22)
	(0,0,22)
	(2,0,24)
	(2,2,24)
	(3,2,42)
	(3,3,42)

[Trail 115]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,23)
	(0,0,23)
	(2,0,25)
	(2,2,25)
	(3,2,43)
	(3,3,43)

[Trail 116]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,24)
	(0,0,24)
	(2,0,26)
	(2,2,26)
	(3,2,44)
	(3,3,44)

[Trail 117]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,25)
	(0,0,25)
	(2,0,27)
	(2,2,27)
	(3,2,45)
	(3,3,45)

[Trail 118]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,26)
	(0,0,26)
	(2,0,28)
	(2,2,28)
	(3,2,46)
	(3,3,46)

[Trail 119]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,27)
	(0,0,27)
	(2,0,29)
	(2,2,29)
	(3,2,47)
	(3,3,47)

[Trail 120]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,28)
	(0,0,28)
	(2,0,30)
	(2,2,30)
	(3,2,48)
	(3,3,48)

[Trail 121]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,29)
	(0,0,29)
	(2,0,31)
	(2,2,31)
	(3,2,49)
	(3,3,49)

[Trail 122]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,30)
	(0,0,30)
	(2,0,32)
	(2,2,32)
	(3,2,50)
	(3,3,50)

[Trail 123]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,31)
	(0,0,31)
	(2,0,33)
	(2,2,33)
	(3,2,51)
	(3,3,51)

[Trail 124]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,32)
	(0,0,32)
	(2,0,34)
	(2,2,34)
	(3,2,52)
	(3,3,52)

[Trail 125]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,33)
	(0,0,33)
	(2,0,35)
	(2,2,35)
	(3,2,53)
	(3,3,53)

[Trail 126]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,34)
	(0,0,34)
	(2,0,36)
	(2,2,36)
	(3,2,54)
	(3,3,54)

[Trail 127]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,35)
	(0,0,35)
	(2,0,37)
	(2,2,37)
	(3,2,55)
	(3,3,55)

[Trail 128]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,36)
	(0,0,36)
	(2,0,38)
	(2,2,38)
	(3,2,56)
	(3,3,56)

[Trail 129]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,37)
	(0,0,37)
	(2,0,39)
	(2,2,39)
	(3,2,57)
	(3,3,57)

[Trail 130]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,38)
	(0,0,38)
	(2,0,40)
	(2,2,40)
	(3,2,58)
	(3,3,58)

[Trail 131]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,39)
	(0,0,39)
	(2,0,41)
	(2,2,41)
	(3,2,59)
	(3,3,59)

[Trail 132]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,40)
	(0,0,40)
	(2,0,42)
	(2,2,42)
	(3,2,60)
	(3,3,60)

[Trail 133]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,41)
	(0,0,41)
	(2,0,43)
	(2,2,43)
	(3,2,61)
	(3,3,61)

[Trail 134]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,42)
	(0,0,42)
	(2,0,44)
	(2,2,44)
	(3,2,62)
	(3,3,62)

[Trail 135]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(0,3,43)
	(0,0,43)
	(2,0,45)
	(2,2,45)
	(3,2,63)
	(3,3,63)

[Trail 136]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,0)
	(1,2,0)
	(2,2,31)
	(2,3,31)
	(4,3,38)
	(4,0,38)

[Trail 137]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,1)
	(1,2,1)
	(2,2,32)
	(2,3,32)
	(4,3,39)
	(4,0,39)

[Trail 138]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,2)
	(1,2,2)
	(2,2,33)
	(2,3,33)
	(4,3,40)
	(4,0,40)

[Trail 139]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,3)
	(1,2,3)
	(2,2,34)
	(2,3,34)
	(4,3,41)
	(4,0,41)

[Trail 140]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,4)
	(1,2,4)
	(2,2,35)
	(2,3,35)
	(4,3,42)
	(4,0,42)

[Trail 141]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,5)
	(1,2,5)
	(2,2,36)
	(2,3,36)
	(4,3,43)
	(4,0,43)

[Trail 142]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,6)
	(1,2,6)
	(2,2,37)
	(2,3,37)
	(4,3,44)
	(4,0,44)

[Trail 143]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,7)
	(1,2,7)
	(2,2,38)
	(2,3,38)
	(4,3,45)
	(4,0,45)

[Trail 144]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,8)
	(1,2,8)
	(2,2,39)
	(2,3,39)
	(4,3,46)
	(4,0,46)

[Trail 145]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,9)
	(1,2,9)
	(2,2,40)
	(2,3,40)
	(4,3,47)
	(4,0,47)

[Trail 146]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,10)
	(1,2,10)
	(2,2,41)
	(2,3,41)
	(4,3,48)
	(4,0,48)

[Trail 147]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,11)
	(1,2,11)
	(2,2,42)
	(2,3,42)
	(4,3,49)
	(4,0,49)

[Trail 148]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,12)
	(1,2,12)
	(2,2,43)
	(2,3,43)
	(4,3,50)
	(4,0,50)

[Trail 149]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,13)
	(1,2,13)
	(2,2,44)
	(2,3,44)
	(4,3,51)
	(4,0,51)

[Trail 150]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,14)
	(1,2,14)
	(2,2,45)
	(2,3,45)
	(4,3,52)
	(4,0,52)

[Trail 151]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,15)
	(1,2,15)
	(2,2,46)
	(2,3,46)
	(4,3,53)
	(4,0,53)

[Trail 152]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,16)
	(1,2,16)
	(2,2,47)
	(2,3,47)
	(4,3,54)
	(4,0,54)

[Trail 153]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,17)
	(1,2,17)
	(2,2,48)
	(2,3,48)
	(4,3,55)
	(4,0,55)

[Trail 154]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,18)
	(1,2,18)
	(2,2,49)
	(2,3,49)
	(4,3,56)
	(4,0,56)

[Trail 155]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,19)
	(1,2,19)
	(2,2,50)
	(2,3,50)
	(4,3,57)
	(4,0,57)

[Trail 156]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,20)
	(1,2,20)
	(2,2,51)
	(2,3,51)
	(4,3,58)
	(4,0,58)

[Trail 157]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,21)
	(1,2,21)
	(2,2,52)
	(2,3,52)
	(4,3,59)
	(4,0,59)

[Trail 158]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,22)
	(1,2,22)
	(2,2,53)
	(2,3,53)
	(4,3,60)
	(4,0,60)

[Trail 159]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,23)
	(1,2,23)
	(2,2,54)
	(2,3,54)
	(4,3,61)
	(4,0,61)

[Trail 160]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,24)
	(1,2,24)
	(2,2,55)
	(2,3,55)
	(4,3,62)
	(4,0,62)

[Trail 161]
	Peso: 36
	Pesi parziali: 12,12,12
	Vertices coordinates (x,y,z):
	(1,0,25)
	(1,2,25)
	(2,2,56)
	(2,3,56)
	(4,3,63)
	(4,0,63)

[Trail 162]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,0)
	(2,0,0)
	(3,0,34)
	(3,3,34)
	(4,3,47)
	(4,4,47)

[Trail 163]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,1)
	(2,0,1)
	(3,0,35)
	(3,3,35)
	(4,3,48)
	(4,4,48)

[Trail 164]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,2)
	(2,0,2)
	(3,0,36)
	(3,3,36)
	(4,3,49)
	(4,4,49)

[Trail 165]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,3)
	(2,0,3)
	(3,0,37)
	(3,3,37)
	(4,3,50)
	(4,4,50)

[Trail 166]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,4)
	(2,0,4)
	(3,0,38)
	(3,3,38)
	(4,3,51)
	(4,4,51)

[Trail 167]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,5)
	(2,0,5)
	(3,0,39)
	(3,3,39)
	(4,3,52)
	(4,4,52)

[Trail 168]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,6)
	(2,0,6)
	(3,0,40)
	(3,3,40)
	(4,3,53)
	(4,4,53)

[Trail 169]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,7)
	(2,0,7)
	(3,0,41)
	(3,3,41)
	(4,3,54)
	(4,4,54)

[Trail 170]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,8)
	(2,0,8)
	(3,0,42)
	(3,3,42)
	(4,3,55)
	(4,4,55)

[Trail 171]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,9)
	(2,0,9)
	(3,0,43)
	(3,3,43)
	(4,3,56)
	(4,4,56)

[Trail 172]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,10)
	(2,0,10)
	(3,0,44)
	(3,3,44)
	(4,3,57)
	(4,4,57)

[Trail 173]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,11)
	(2,0,11)
	(3,0,45)
	(3,3,45)
	(4,3,58)
	(4,4,58)

[Trail 174]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,12)
	(2,0,12)
	(3,0,46)
	(3,3,46)
	(4,3,59)
	(4,4,59)

[Trail 175]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,13)
	(2,0,13)
	(3,0,47)
	(3,3,47)
	(4,3,60)
	(4,4,60)

[Trail 176]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,14)
	(2,0,14)
	(3,0,48)
	(3,3,48)
	(4,3,61)
	(4,4,61)

[Trail 177]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,15)
	(2,0,15)
	(3,0,49)
	(3,3,49)
	(4,3,62)
	(4,4,62)

[Trail 178]
	Peso: 35
	Pesi parziali: 12,12,11
	Vertices coordinates (x,y,z):
	(2,4,16)
	(2,0,16)
	(3,0,50)
	(3,3,50)
	(4,3,63)
	(4,4,63)

